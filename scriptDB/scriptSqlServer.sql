
CREATE DATABASE pruebaTecnicaIT
GO

USE pruebaTecnicaIT
GO

CREATE TABLE producto(
	codigo varchar(50) NOT NULL primary key,
	nombre varchar(50) NOT NULL,
	idEstado int NOT NULL,
)