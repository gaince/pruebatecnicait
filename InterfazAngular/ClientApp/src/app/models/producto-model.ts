import { EstadoModel } from "./estado-model";

export class ProductoModel {
    Codigo : string;
    Nombre: string;
    IdEstado: EstadoModel
}