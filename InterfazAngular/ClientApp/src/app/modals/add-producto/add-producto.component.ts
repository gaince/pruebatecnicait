import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductoModel } from 'src/app/models/producto-model';
import { ProductoService } from 'src/app/services/producto.service';
import Swal from 'sweetalert2';
import EstadoJson from 'src/app/json/estados.json'; 

@Component({
  selector: 'app-add-producto',
  templateUrl: './add-producto.component.html',
  styleUrls: ['./add-producto.component.css']
})
export class AddProductoComponent implements OnInit {
  formProduct: FormGroup;
  productoNuevo = new ProductoModel();
  listStatus = EstadoJson;
  constructor(private productService: ProductoService, private fb:FormBuilder, private dialogRef: MatDialogRef<AddProductoComponent>) {
    this.formProduct = this.fb.group({
      codigo: new FormControl("", Validators.required),
      nombre: new FormControl("",Validators.required),
      estado: new FormControl("", Validators.required)
    })
   }

  ngOnInit(): void { 
  }
   
  setearCodigo(valor){
    this.productoNuevo.IdEstado = valor;
  }

  isExistCodeProduct(){
    this.productoNuevo.Codigo = this.formProduct.get("codigo").value;
    this.productoNuevo.Nombre = this.formProduct.get("nombre").value;  
    this.productService.IsExistCodeProduct(this.productoNuevo.Codigo).subscribe((response:any)=>{
      if(!response){
        Swal.fire({
          title: 'Guardando producto',  
          didOpen:()=>{
            Swal.showLoading();
          }
        }); 
          this.productService.AddProduct(this.productoNuevo).subscribe((response)=>{
            Swal.fire({
              title: 'Producto guardado.',  
              icon:"success"
            });
            this.dialogRef.close("update");
          },err=>{
            Swal.fire({
              title: 'Error al guardar el producto.',  
              icon:"error"
            });
          });
      }else{
        Swal.fire({
          title: 'El código del producto ya existe.',  
          icon:"error"
        }); 
      }
    })
  }

}
