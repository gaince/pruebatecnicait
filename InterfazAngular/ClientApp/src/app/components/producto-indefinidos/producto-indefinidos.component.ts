import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AddProductoComponent } from 'src/app/modals/add-producto/add-producto.component';
import { ProductoModel } from 'src/app/models/producto-model';
import { ProductoService } from 'src/app/services/producto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-producto-indefinidos',
  templateUrl: './producto-indefinidos.component.html',
  styleUrls: ['./producto-indefinidos.component.css']
})
export class ProductoIndefinidosComponent implements OnInit {
  displayedColumns: string[] = ['codigo', 'name', 'tipo', 'editar'];
  dataSource;
  listEditProduct = [];
  formProduct: FormGroup;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private productService: ProductoService, private dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.getUndefinedProduct();
  }

  saveProductEdit(product, nuevoEstado) {
    product.idEstado = parseInt(nuevoEstado);
    this.listEditProduct = this.listEditProduct.filter(x => x.codigo != product.codigo);
    this.listEditProduct.push(product);
  }

  getUndefinedProduct() {
    this.productService.GetUndefinedProduct().subscribe((response: any) => {
      if (response.length > 0) {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
      } else {
        this.dataSource = [];
        Swal.fire({
          title: 'No hay productos sin definir estados.',
          icon: "warning"
        });
      }
    })
  }

  openModalAddProduct() {
    const dialogRef = this.dialog.open(AddProductoComponent, {
      width: "60vw",
      height: "auto",
      position: { top: "40px" }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getUndefinedProduct();
    })
  }

  editStatusProduct() {
    Swal.fire({
      title: 'Modificando estados de los productos.',
      didOpen: () => {
        Swal.showLoading();
      }
    });
    this.productService.EditStatusProduct(this.listEditProduct).subscribe((response: any) => {
      Swal.fire({
        title: 'Productos editados.',
        icon: "success"
      });
      this.getUndefinedProduct();
    }, err => {
      Swal.fire({
        title: 'Error al editar algunos productos.',
        icon: "error"
      });
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
