import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoIndefinidosComponent } from './producto-indefinidos.component';

describe('ProductoIndefinidosComponent', () => {
  let component: ProductoIndefinidosComponent;
  let fixture: ComponentFixture<ProductoIndefinidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoIndefinidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoIndefinidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
