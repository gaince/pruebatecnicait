import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoDefectuosoComponent } from './producto-defectuoso.component';

describe('ProductoDefectuosoComponent', () => {
  let component: ProductoDefectuosoComponent;
  let fixture: ComponentFixture<ProductoDefectuosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoDefectuosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoDefectuosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
