import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoOptimoComponent } from './producto-optimo.component';

describe('ProductoOptimoComponent', () => {
  let component: ProductoOptimoComponent;
  let fixture: ComponentFixture<ProductoOptimoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoOptimoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoOptimoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
