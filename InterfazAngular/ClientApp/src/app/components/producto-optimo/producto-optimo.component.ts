import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductoService } from 'src/app/services/producto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-producto-optimo',
  templateUrl: './producto-optimo.component.html',
  styleUrls: ['./producto-optimo.component.css']
})
export class ProductoOptimoComponent implements OnInit {
  displayedColumns: string[] = ['codigo', 'name', 'vender'];
  dataSource;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private productService:ProductoService) { }

  ngOnInit(): void {
    this.getOptimalProduct();
  }

  getOptimalProduct(){ 
    this.productService.GetOptimalProduct().subscribe((response:any)=>{ 
      if(response.length > 0){ 
        this.dataSource = new MatTableDataSource(response); 
        this.dataSource.paginator = this.paginator;
      }else{
        this.dataSource = [];
        Swal.fire({
            title: 'No hay productos marcados como optimos.',  
            icon:"warning"
          });
      }
    })
  }
  
  venderProduct(codigoProducto){
    Swal.fire({
      title: 'Vendiendo producto',  
      didOpen:()=>{
        Swal.showLoading();
      }
    }); 
    this.productService.VenderProduct(codigoProducto).subscribe((response:any)=>{  
        Swal.fire({
          title: 'Productos vendido.',  
          icon:"success"
        }); 
        debugger
        this.getOptimalProduct();
    },err=>{
      Swal.fire({
        title: 'No se pudo vender el producto.',  
        icon:"error"
      });
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
