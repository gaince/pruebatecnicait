import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductoService } from 'src/app/services/producto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-producto-vendido',
  templateUrl: './producto-vendido.component.html',
  styleUrls: ['./producto-vendido.component.css']
})
export class ProductoVendidoComponent implements OnInit {

  displayedColumns: string[] = ['codigo', 'name'];
  dataSource;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private productService: ProductoService) { }

  ngOnInit(): void {
    this.getSoldProduct();
  }

  getSoldProduct() {
    this.productService.GetSoldProduct().subscribe((response: any) => {
      if (response.length > 0) {
        this.dataSource = new MatTableDataSource(response); 
        this.dataSource.paginator = this.paginator;
      } else {
        this.dataSource = [];
        Swal.fire({
          title: 'No hay productos vendidos.',
          icon: "warning"
        });
      }
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
