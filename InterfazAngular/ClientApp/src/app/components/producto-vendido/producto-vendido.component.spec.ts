import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoVendidoComponent } from './producto-vendido.component';

describe('ProductoVendidoComponent', () => {
  let component: ProductoVendidoComponent;
  let fixture: ComponentFixture<ProductoVendidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoVendidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoVendidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
