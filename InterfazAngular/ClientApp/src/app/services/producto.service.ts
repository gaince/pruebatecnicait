import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ProductoModel } from '../models/producto-model';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private _http:HttpClient) { }

  AddProduct(product: ProductoModel){
    return this._http.post(environment.apiURL+ "api/Product/AddProduct", product);
  }
  
  GetDefectiveProduct(){
    return this._http.get(environment.apiURL+ "api/Product/GetDefectiveProduct");
  }

  GetOptimalProduct(){
    return this._http.get(environment.apiURL+ "api/Product/GetOptimalProduct");
  }

  GetUndefinedProduct(){
    return this._http.get(environment.apiURL+ "api/Product/GetUndefinedProduct");
  }

  GetSoldProduct(){
    return this._http.get(environment.apiURL+ "api/Product/GetSoldProduct");
  }
 
  IsExistCodeProduct(Codigo:string){ 
    return this._http.get(environment.apiURL+ "api/Product/IsExistCodeProduct/"+ Codigo);
  }

  VenderProduct(Codigo:string){ 
    return this._http.get(environment.apiURL+ "api/Product/VenderProduct/"+ Codigo);
  }

  EditStatusProduct(products){
    return this._http.post(environment.apiURL+ "api/Product/EditStatusProduct", products);
  }

}
