﻿using pruebaTecnicaFinal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaTecnicaFinal.Interface
{
    interface IProduct
    {
        // Agrega los productos al inventario
        void AddProduct(Producto producto);
        // Obtiene el listado de los productos defectuosos
        List<Producto> GetDefectiveProduct();
        // Obtiene el listado de los productos optimos
        List<Producto> GetOptimalProduct();
        // Obtiene el listado de los productos sin identificar sin son optimos o no
        List<Producto> GetUndefinedProduct();
        // Obtiene el listado de los productos vendidos
        List<Producto> GetSoldProduct();
    }
}
