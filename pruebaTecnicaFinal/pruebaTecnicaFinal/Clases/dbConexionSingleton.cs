﻿using pruebaTecnicaFinal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaTecnicaFinal.Clases
{
    public class dbConexionSingleton
    {
        private static pruebaTecnicaITContext _instance;
        private dbConexionSingleton() { }

        public static pruebaTecnicaITContext getIntance()
        {
            if (_instance == null)
            {
                _instance = new pruebaTecnicaITContext();
            }
            return _instance;
        }

    }
}
