﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pruebaTecnicaFinal.Clases;
using pruebaTecnicaFinal.Interface;
using pruebaTecnicaFinal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaTecnicaFinal.Controllers
{
    [Route("api/[controller]/[action]")] 
    public class ProductController : IProduct
    {
        pruebaTecnicaITContext _db = dbConexionSingleton.getIntance();

        // Agrega los productos al inventario
        [HttpPost]
        public void AddProduct([FromBody] Producto producto)
        {
            _db.Producto.Add(producto);
            _db.SaveChanges();
        }
        // Obtiene el listado de los productos defectuosos
        [HttpGet]
        public List<Producto> GetDefectiveProduct()
        {
            return _db.Producto.Where(x => x.IdEstado == 1).ToList();
        }
        // Obtiene el listado de los productos optimos
        [HttpGet]
        public List<Producto> GetOptimalProduct()
        {
            return _db.Producto.Where(x => x.IdEstado == 2).ToList();
        }
        // Obtiene el listado de los productos sin identificar sin son optimos o no
        [HttpGet]
        public List<Producto> GetUndefinedProduct()
        {
            return _db.Producto.Where(x => x.IdEstado == 3).ToList();
        }
        // Obtiene el listado de los productos vendidos
        [HttpGet]
        public List<Producto> GetSoldProduct()
        {
            return _db.Producto.Where(x => x.IdEstado == 5).ToList();
        }

        [HttpGet("{codigo}")]
        public bool IsExistCodeProduct(string codigo)
        {
            return _db.Producto.Where(x => x.Codigo == codigo).Any();
        }

        [HttpGet("{codigo}")]
        public void VenderProduct(string codigo)
        {
            var dataProduct = _db.Producto.Where(x => x.Codigo == codigo).FirstOrDefault();
            dataProduct.IdEstado = 5;
            _db.SaveChanges();
        }


        [HttpPost]
        public void EditStatusProduct([FromBody] List<Producto> product)
        {
            foreach (var item in product)
            {
                var dataProduct = _db.Producto.Where(x => x.Codigo == item.Codigo).FirstOrDefault();
                dataProduct.IdEstado = item.IdEstado;
            } 
            _db.SaveChanges();
        }

    }
}
